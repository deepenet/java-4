package threadpool;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {
    private List<Thread> list;

    public ThreadPool() {
        list = new ArrayList<>();
    }
    public void addTask(Runnable task) {
        list.add(new Thread(task));
    }
    private void interruptAll() {
        for(int i = 0; i < list.size(); ++i) {
            (list.get(i)).interrupt();
            System.out.println(i+1 + " interrupted");
        }
    }
    public void removeAll() {
        interruptAll();
        list.clear();
    }
    public void runAll() {
        for (Thread thread : list) {
            if (!thread.isAlive()) {
                thread.start();
            }
        }
    }
}
