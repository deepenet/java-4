import factory.backend.Logic;
import factory.frontend.MainFrame;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
    // load properties
        Properties properties = new Properties();
        try {
            properties.load(new FileReader("src/resources/properties.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Logic logic = new Logic(properties);
        MainFrame mainFrame = new MainFrame();
        mainFrame.initialiseControl(logic,
                Integer.parseInt(properties.getProperty("CarLinkers")),
                Integer.parseInt(properties.getProperty("AccessorySuppliers")),
                Integer.parseInt(properties.getProperty("CarDealers")),
                Boolean.parseBoolean(properties.getProperty("LogSale")));
        mainFrame.setVisible(true);
        logic.setMainFrame(mainFrame);
        logic.runSimulation();
    }
}
