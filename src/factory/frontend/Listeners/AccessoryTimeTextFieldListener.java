package factory.frontend.Listeners;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccessoryTimeTextFieldListener implements ActionListener {
    private JTextField textField;
    private JSlider slider;
    public AccessoryTimeTextFieldListener(JSlider slider, JTextField textField) {
        this.slider = slider;
        this.textField = textField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        slider.setValue(Integer.parseInt(textField.getText()));
    }
}
