package factory.frontend.Listeners;

import factory.backend.Logic;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ApplyChangesListener implements ActionListener {
    private JSlider accessorySlider;
    private JSlider bodySlider;
    private JSlider motorSlider;
    private JSlider carSlider;
    private JSlider dealerSlider;
    private Logic logic;
    public ApplyChangesListener(Logic logic, JSlider accessorySlider, JSlider bodySlider, JSlider motorSlider, JSlider carSlider, JSlider dealerSlider) {
        this.logic = logic;
        this.accessorySlider = accessorySlider;
        this.bodySlider = bodySlider;
        this.motorSlider = motorSlider;
        this.carSlider = carSlider;
        this.dealerSlider = dealerSlider;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        logic.setAccessorySuppliersDelay(accessorySlider.getValue());
        logic.setBodySupplierDelay(bodySlider.getValue());
        logic.setMotorSupplierDelay(motorSlider.getValue());
        logic.setCarLinkersDelay(carSlider.getValue());
        logic.setDealersDelay(dealerSlider.getValue());
    }
}
