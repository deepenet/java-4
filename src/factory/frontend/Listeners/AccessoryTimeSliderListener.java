package factory.frontend.Listeners;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AccessoryTimeSliderListener implements ChangeListener {
    private JTextField textField;
    private JSlider slider;
    public AccessoryTimeSliderListener(JSlider slider, JTextField textField) {
        this.slider = slider;
        this.textField = textField;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        textField.setText(Integer.toString(slider.getValue()));
    }
}
