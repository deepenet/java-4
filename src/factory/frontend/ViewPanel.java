package factory.frontend;

import javax.swing.*;

public class ViewPanel extends JPanel {
    private JPanel mainPanel;
    private JLabel bodySupplierStatus;
    private JLabel bodySupplierProduced;
    private JLabel bodyStorageSize;
    private JLabel bodyStorageMaxSize;
    private JLabel motorSupplierStatus;
    private JLabel motorSupplierProduced;
    private JLabel motorStorageSize;
    private JLabel motorStorageMaxSize;
    private JLabel accessoryStorageSize;
    private JLabel accessoryStorageMaxSize;
    private JLabel workingAccessorySuppliersNumb;
    private JLabel waitingAccessorySuppliersNumb;
    private JLabel accessorySupplierProduced;
    private JLabel workingCarLinkersNumb;
    private JLabel sleepingCarLinkersNumb;
    private JLabel carLinkersProduced;
    private JLabel carStorageSize;
    private JLabel carStorageMaxSize;
    private JLabel workingDealersNumb;
    private JLabel waitingDealersNumb;
    private JLabel dealersProduced;
    ViewPanel() {
        add(mainPanel);
    }
    void initialize(int accessoryStorageSize, int bodyStorageSize, int motorStorageSize, int carStorageSize) {
        carStorageMaxSize.setText(Integer.toString(carStorageSize));
        accessoryStorageMaxSize.setText(Integer.toString(accessoryStorageSize));
        bodyStorageMaxSize.setText(Integer.toString(bodyStorageSize));
        motorStorageMaxSize.setText(Integer.toString(motorStorageSize));
    }
    void setBodySupplierStatus(boolean value) {
        bodySupplierStatus.setText(value ? "Work" : "Wait");
    }
    void setBodySupplierProduced(int value) {
        bodySupplierProduced.setText(Integer.toString(value));
    }
    void setBodyStorageSize(int value) {
        bodyStorageSize.setText(Integer.toString(value));
    }
    void setMotorSupplierStatus(boolean value) {
        motorSupplierStatus.setText(value ? "Work" : "Wait");
    }
    void setMotorSupplierProduced(int value) {
        motorSupplierProduced.setText(Integer.toString(value));
    }
    void setMotorStorageSize(int value) {
        motorStorageSize.setText(Integer.toString(value));
    }
    void setAccessoryStorageSize(int value) {
        accessoryStorageSize.setText(Integer.toString(value));
    }
    void setWorkingAccessorySuppliersNumb(int value) {
        workingAccessorySuppliersNumb.setText(Integer.toString(value));
    }
    void setWaitingAccessorySuppliersNumb(int value) {
        waitingAccessorySuppliersNumb.setText(Integer.toString(value));
    }
    void setAccessorySupplierProduced(int value) {
        accessorySupplierProduced.setText(Integer.toString(value));
    }
    void setWorkingCarLinkersNumb(int value) {
        workingCarLinkersNumb.setText(Integer.toString(value));
    }
    void setSleepingCarLinkersNumb(int value) {
        sleepingCarLinkersNumb.setText(Integer.toString(value));
    }
    void setCarLinkersProduced(int value) {
        carLinkersProduced.setText(Integer.toString(value));
    }
    void setCarStorageSize(int value) {
        carStorageSize.setText(Integer.toString(value));
    }
    void setWorkingDealersNumb(int value) {
        workingDealersNumb.setText(Integer.toString(value));
    }
    void setWaitingDealersNumb(int value) {
        waitingDealersNumb.setText(Integer.toString(value));
    }
    void setDealersProduced(int value) {
        dealersProduced.setText(Integer.toString(value));
    }
}
