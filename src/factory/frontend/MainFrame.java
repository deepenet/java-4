package factory.frontend;

import factory.backend.Logic;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private JPanel controlPanel;
    private JPanel viewPanel;
    private JPanel mainPanel;
    private Logic logic;
// methods
    public MainFrame() {
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        controlPanel.setLayout(new GridLayout(1, 1));
        viewPanel.setLayout(new GridLayout(1, 1));
        controlPanel.add(new ControlPanel());
        viewPanel.add(new ViewPanel());
        setSize(800, 400);
        add(mainPanel);
    }
    @Override
    public void dispose() {
        logic.interrupt();
        super.dispose();
    }
    public void initialiseControl(Logic logic, int carLinkersNumb, int accessorySuppliersNumb, int dealersNumb, boolean isLogOn) {
        this.logic = logic;
        ((ControlPanel)(controlPanel.getComponent(0))).initialize(logic, carLinkersNumb, accessorySuppliersNumb, dealersNumb, isLogOn);
    }
    public void initializeView(int accessoryStorageSize, int bodyStorageSize, int motorStorageSize, int carStorageSize) {
        ((ViewPanel)(viewPanel.getComponent(0))).initialize(accessoryStorageSize, bodyStorageSize, motorStorageSize, carStorageSize);
    }
    public void setVisible(boolean b) {
        super.setVisible(b);
    }
    public synchronized void setBodySupplierStatus(boolean value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setBodySupplierStatus(value);
    }
    public synchronized void setBodySupplierProduced(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setBodySupplierProduced(value);
    }
    public synchronized void setBodyStorageSize(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setBodyStorageSize(value);
    }
    public synchronized void setMotorSupplierStatus(boolean value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setMotorSupplierStatus(value);
    }
    public synchronized void setMotorSupplierProduced(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setMotorSupplierProduced(value);
    }
    public synchronized void setMotorStorageSize(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setMotorStorageSize(value);
    }
    public synchronized void setAccessoryStorageSize(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setAccessoryStorageSize(value);
    }
    public synchronized void setWorkingAccessorySuppliersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setWorkingAccessorySuppliersNumb(value);
    }
    public synchronized void setWaitingAccessorySuppliersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setWaitingAccessorySuppliersNumb(value);
    }
    public synchronized void setAccessorySupplierProduced(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setAccessorySupplierProduced(value);
    }
    public synchronized void setWorkingCarLinkersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setWorkingCarLinkersNumb(value);
    }
    public synchronized void setSleepingCarLinkersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setSleepingCarLinkersNumb(value);
    }
    public synchronized void setCarLinkersProduced(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setCarLinkersProduced(value);
    }
    public synchronized void setCarStorageSize(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setCarStorageSize(value);
    }
    public synchronized void setWorkingDealersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setWorkingDealersNumb(value);
    }
    public synchronized void setWaitingDealersNumb(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setWaitingDealersNumb(value);
    }
    public synchronized void setDealersProduced(int value) {
        ((ViewPanel)(viewPanel.getComponent(0))).setDealersProduced(value);
    }
}
