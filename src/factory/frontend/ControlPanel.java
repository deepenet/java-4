package factory.frontend;

import factory.backend.Logic;
import factory.frontend.Listeners.AccessoryTimeSliderListener;
import factory.frontend.Listeners.AccessoryTimeTextFieldListener;
import factory.frontend.Listeners.ApplyChangesListener;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;

class ControlPanel extends JPanel {
    private JSlider accessoryTimeSlider;
    private JSlider bodyTimeSlider;
    private JSlider motorTimeSlider;
    private JSlider carTimeSlider;
    private JSlider dealerTimeSlider;
    private JLabel carLinkersNumb;
    private JLabel accessorySuppliersNumb;
    private JLabel carDealersNumb;
    private JLabel isLogOn;
    private JButton commit;
    private JPanel mainPanel;
    private JPanel accessoryTimePanel;
    private JPanel bodyTimePanel;
    private JPanel motorTimePanel;
    private JPanel carTimePanel;
    private JPanel dealerTimePanel;

    ControlPanel() {
        add(mainPanel);
    }
    void initialize(Logic logic, int carLinkersNumb, int accessorySuppliersNumb, int dealersNumb, boolean isLogOn) {
        this.carLinkersNumb.setText(Integer.toString(carLinkersNumb));
        this.accessorySuppliersNumb.setText(Integer.toString(accessorySuppliersNumb));
        this.carDealersNumb.setText(Integer.toString(dealersNumb));
        this.isLogOn.setText(Boolean.toString(isLogOn));

        configureSliderAndTextField(accessoryTimeSlider, accessoryTimePanel);
        configureSliderAndTextField(bodyTimeSlider, bodyTimePanel);
        configureSliderAndTextField(motorTimeSlider, motorTimePanel);
        configureSliderAndTextField(carTimeSlider, carTimePanel);
        configureSliderAndTextField(dealerTimeSlider, dealerTimePanel);

        commit.addActionListener(new ApplyChangesListener(logic, accessoryTimeSlider, bodyTimeSlider, motorTimeSlider, carTimeSlider, dealerTimeSlider));
    }
    private void configureSliderAndTextField(JSlider slider, JPanel panel) {
        JTextField textField = new JTextField();
        textField.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if(str.length() + getLength() > 5) {
                    return;
                }
                for(int i = 0; i < str.length(); ++i) {
                    if(!Character.isDigit(str.charAt(i))) {
                        return;
                    }
                }
                super.insertString(offs, str, a);
            }
        });
        textField.setText(Integer.toString(slider.getValue()));
        panel.setLayout(new GridLayout(1, 1));
        panel.add(textField);
        slider.addChangeListener(new AccessoryTimeSliderListener(slider, textField));
        textField.addActionListener(new AccessoryTimeTextFieldListener(slider, textField));
    }

}
