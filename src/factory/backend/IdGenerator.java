package factory.backend;

import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator {
    private AtomicInteger id;
    public IdGenerator() {
        id = new AtomicInteger(0);
    }
    public int getId() {
        return id.getAndIncrement();
    }
}
