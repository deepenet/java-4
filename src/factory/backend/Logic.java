package factory.backend;

import factory.backend.Storages.AccessoryStorage;
import factory.backend.Storages.BodyStorage;
import factory.backend.Storages.CarStorage;
import factory.backend.Storages.MotorStorage;
import factory.backend.Workers.*;
import factory.frontend.MainFrame;
import threadpool.ThreadPool;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Logic {
    private MainFrame mainFrame;
    private Properties properties;
    private List<AccessorySupplier> accessorySuppliersList;
    private List<CarLinker> carLinkersList;
    private List<CarDealer> dealersList;
    private BodySupplier bodySupplier;
    private MotorSupplier motorSupplier;
    private ThreadPool threadPool;
    public Logic(Properties properties) {
        this.properties = properties;
        accessorySuppliersList = new ArrayList<>();
        carLinkersList = new ArrayList<>();
        dealersList = new ArrayList<>();
    }
    public void setMainFrame(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }
    public void runSimulation() {
    // open log file if it is needed
        FileWriter fileWriter = null;
        if(Boolean.parseBoolean(properties.getProperty("LogSale"))) {
            try {
                File file = new File("src/resources/logFile.inf");
                file.createNewFile(); // if file exist it will do nothing
                fileWriter = new FileWriter(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    // set storage size in view
        mainFrame.initializeView(
                Integer.parseInt(properties.getProperty("AccessoryStorageSize")),
                Integer.parseInt(properties.getProperty("BodyStorageSize")),
                Integer.parseInt(properties.getProperty("MotorStorageSize")),
                Integer.parseInt(properties.getProperty("CarStorageSize"))
        );
    // create storage
        AccessoryStorage accessoryStorage = new AccessoryStorage(Integer.parseInt(properties.getProperty("AccessoryStorageSize")), mainFrame);
        BodyStorage bodyStorage = new BodyStorage(Integer.parseInt(properties.getProperty("BodyStorageSize")), mainFrame);
        MotorStorage motorStorage = new MotorStorage(Integer.parseInt(properties.getProperty("MotorStorageSize")), mainFrame);
        CarStorage carStorage = new CarStorage(Integer.parseInt(properties.getProperty("CarStorageSize")), mainFrame);
    // create car selling controller
        SellCarController sellCarController = new SellCarController(carStorage, fileWriter);
    // create and fill thread pool
        threadPool = new ThreadPool();
        Worker temporaryWorker;
     // add dealers
        IdGenerator idGenerator = new IdGenerator();
        WorkersUnion workersUnion = new WorkersUnion(Integer.parseInt(properties.getProperty("CarDealers")));
        for(int i = 0; i < Integer.parseInt(properties.getProperty("CarDealers")); ++i) {
            temporaryWorker = new CarDealer(i + 1, idGenerator, sellCarController, mainFrame, workersUnion);
            temporaryWorker.setDelay(2000);
            dealersList.add((CarDealer) temporaryWorker);
            threadPool.addTask((Runnable) temporaryWorker);
        }
     // add accessory suppliers
        idGenerator = new IdGenerator();
        workersUnion = new WorkersUnion(Integer.parseInt(properties.getProperty("AccessorySuppliers")));
        for(int i = 0; i < Integer.parseInt(properties.getProperty("AccessorySuppliers")); ++i) {
            temporaryWorker = new AccessorySupplier(idGenerator, accessoryStorage, mainFrame, workersUnion);
            temporaryWorker.setDelay(2000);
            accessorySuppliersList.add((AccessorySupplier) temporaryWorker);
            threadPool.addTask((Runnable) temporaryWorker);
        }
     // add body supplier
        bodySupplier = new BodySupplier(new IdGenerator(), bodyStorage, mainFrame);
        bodySupplier.setDelay(200);
        threadPool.addTask(bodySupplier);
     // add motor supplier
        motorSupplier= new MotorSupplier(new IdGenerator(), motorStorage, mainFrame);
        motorSupplier.setDelay(200);
        threadPool.addTask(motorSupplier);
     // add car linkers
        idGenerator = new IdGenerator();
        workersUnion = new WorkersUnion(Integer.parseInt(properties.getProperty("CarLinkers")));
        for(int i = 0; i < Integer.parseInt(properties.getProperty("CarLinkers")); ++i) {
            temporaryWorker = new CarLinker(idGenerator, accessoryStorage, bodyStorage, motorStorage, carStorage, mainFrame, workersUnion);
            temporaryWorker.setDelay(2000);
            carLinkersList.add((CarLinker) temporaryWorker);
            threadPool.addTask((Runnable) temporaryWorker);
        }
        threadPool.runAll();
    }
    public void interrupt() {
        threadPool.removeAll();
    }
    public void setAccessorySuppliersDelay(int value) {
        for(AccessorySupplier accessorySupplier : accessorySuppliersList) {
            accessorySupplier.setDelay(value);
        }
    }
    public void setCarLinkersDelay(int value) {
        for(CarLinker carLinker : carLinkersList) {
            carLinker.setDelay(value);
        }
    }
    public void setDealersDelay(int value) {
        for(CarDealer dealer : dealersList) {
            dealer.setDelay(value);
        }
    }
    public void setBodySupplierDelay(int value) {
        bodySupplier.setDelay(value);
    }
    public void setMotorSupplierDelay(int value) {
        motorSupplier.setDelay(value);
    }
}
