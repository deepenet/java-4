package factory.backend.Storages;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.Products.Body;
import factory.backend.Products.Product;
import factory.frontend.MainFrame;

public class BodyStorage extends Storage {
    public BodyStorage(int maxSize, MainFrame mainFrame) {
        super(maxSize, mainFrame);
    }
    @Override
    public void putProduct(Product product) throws InterruptedException, IncorrectProductTypeException {
        if(product.getClass() != Body.class) {
            throw new IncorrectProductTypeException();
        }
        super.putProduct(product);
    }

    @Override
    void setSize() {
        mainFrame.setBodyStorageSize(size());
    }
}
