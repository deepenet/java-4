package factory.backend.Storages;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.Products.Motor;
import factory.backend.Products.Product;
import factory.frontend.MainFrame;

public class MotorStorage extends Storage {
    public MotorStorage(int maxSize, MainFrame mainFrame) {
        super(maxSize, mainFrame);
    }

    @Override
    public synchronized void putProduct(Product product) throws InterruptedException, IncorrectProductTypeException {
        if(product.getClass() != Motor.class) {
            throw new IncorrectProductTypeException();
        }
        super.putProduct(product);
    }

    @Override
    void setSize() {
        mainFrame.setMotorStorageSize(size());
    }
}
