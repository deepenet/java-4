package factory.backend.Storages;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.Products.Accessory;
import factory.backend.Products.Product;
import factory.frontend.MainFrame;

public class AccessoryStorage extends Storage {
    public AccessoryStorage(int maxSize, MainFrame mainFrame) {
        super(maxSize, mainFrame);
    }

    @Override
    public synchronized void putProduct(Product product) throws InterruptedException, IncorrectProductTypeException {
        if(product.getClass() != Accessory.class) {
            throw new IncorrectProductTypeException();
        }
        super.putProduct(product);
    }

    @Override
    void setSize() {
        mainFrame.setAccessoryStorageSize(size());
    }
}
