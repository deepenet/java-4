package factory.backend.Storages;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.Products.Product;
import factory.frontend.MainFrame;

import java.util.ArrayList;
import java.util.List;

public abstract class Storage {
// fields
    private int maxSize;
    protected MainFrame mainFrame;
    protected List<Product> products;
// methods
    Storage(int maxSize, MainFrame mainFrame) {
        this.maxSize = maxSize;
        products = new ArrayList<>();
        this.mainFrame = mainFrame;
    }
    public synchronized void putProduct(Product product) throws InterruptedException, IncorrectProductTypeException {
        if(maxSize > products.size()) {
            products.add(product);
            setSize();
        } else {
            this.wait();
            this.putProduct(product);
        }
        this.notify();
    }
    public synchronized Product getProduct() throws InterruptedException {
        if (products.size() == 0) {
            this.wait();
            return this.getProduct();
        } else {
            Product out = products.get(products.size() - 1);
            products.remove(products.size() - 1);
            setSize();
            this.notify();
            return out;
        }
    }
    int size() {
        return products.size();
    }
    abstract void setSize();
}
