package factory.backend.Storages;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.Products.Car;
import factory.backend.Products.Product;
import factory.frontend.MainFrame;

public class CarStorage extends Storage {
    public CarStorage(int maxSize, MainFrame mainFrame) {
        super(maxSize, mainFrame);
    }
    @Override
    public synchronized void putProduct(Product product) throws InterruptedException, IncorrectProductTypeException {
        if(product.getClass() != Car.class) {
            throw new IncorrectProductTypeException();
        }
        super.putProduct(product);
    }

    @Override
    void setSize() {
        mainFrame.setCarStorageSize(size());
    }
}
