package factory.backend.Products;

public class Car extends Product {
    private Accessory accessory;
    private Body body;
    private Motor motor;
    public Car(int id, Accessory accessory, Body body, Motor motor) {
        super(id);
        this.accessory = accessory;
        this.body = body;
        this.motor = motor;
    }
    public Accessory getAccessory() {
        return accessory;
    }
    public Body getBody() {
        return body;
    }
    public Motor getMotor() {
        return motor;
    }
}
