package factory.backend.Products;

public abstract class Product {
    private int id;
    Product(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
}
