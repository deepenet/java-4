package factory.backend.Workers;

import factory.backend.IdGenerator;
import factory.backend.Products.Body;
import factory.backend.Products.Product;
import factory.backend.Storages.BodyStorage;
import factory.frontend.MainFrame;

public class BodySupplier extends RememberingSupplier {
    public BodySupplier(IdGenerator idGenerator, BodyStorage storage, MainFrame mainFrame) {
        super(idGenerator, storage, mainFrame);
    }

    @Override
    Product doWork() throws InterruptedException {
        super.rememberWorking();
        Thread.sleep(super.delay);
        //System.out.println("Body created");
        super.stopWorking();
        return new Body(idGenerator.getId());
    }

    @Override
    void pushChanges() {
        mainFrame.setBodySupplierProduced(produced);
        mainFrame.setBodySupplierStatus(isWorking);
    }
}
