package factory.backend.Workers;

import java.util.concurrent.atomic.AtomicInteger;

public class WorkersUnion {
    private AtomicInteger waitersNumb;
    private AtomicInteger workersNumb;
    private AtomicInteger produced;
    public WorkersUnion(int wholeNumb) {
        this.waitersNumb = new AtomicInteger(wholeNumb);
        this.workersNumb = new AtomicInteger(0);
        this.produced = new AtomicInteger(0);
    }
    void workerStart() {
        waitersNumb.decrementAndGet();
        workersNumb.incrementAndGet();
    }
    void workerStop() {
        workersNumb.decrementAndGet();
        waitersNumb.incrementAndGet();
        produced.incrementAndGet();
    }
    int getWaitersNumb() {
        return waitersNumb.get();
    }
    int getWorkersNumb() {
        return workersNumb.get();
    }
    int getProduced() {
        return produced.get();
    }
}
