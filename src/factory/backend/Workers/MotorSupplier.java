package factory.backend.Workers;

import factory.backend.IdGenerator;
import factory.backend.Products.Motor;
import factory.backend.Products.Product;
import factory.backend.Storages.MotorStorage;
import factory.frontend.MainFrame;


public class MotorSupplier extends RememberingSupplier {
    public MotorSupplier(IdGenerator idGenerator, MotorStorage storage, MainFrame mainFrame) {
        super(idGenerator, storage, mainFrame);
    }

    @Override
    Product doWork() throws InterruptedException {
        super.rememberWorking();
        Thread.sleep(super.delay);
        //System.out.println("Motor created");
        super.stopWorking();
        return new Motor(idGenerator.getId());
    }

    @Override
    void pushChanges() {
        mainFrame.setMotorSupplierProduced(produced);
        mainFrame.setMotorSupplierStatus(isWorking);
    }
}
