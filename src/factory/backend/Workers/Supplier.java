package factory.backend.Workers;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.IdGenerator;
import factory.backend.Products.Product;
import factory.backend.Storages.Storage;
import factory.frontend.MainFrame;

public abstract class Supplier extends Worker implements Runnable {
    protected Storage storage;
    Supplier(IdGenerator idGenerator, Storage storage, MainFrame mainFrame) {
        super(idGenerator, mainFrame);
        this.storage = storage;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                storage.putProduct(doWork());
            } catch (InterruptedException e) {
                return;
            } catch (IncorrectProductTypeException ignored) {}
        }
    }

    abstract Product doWork() throws InterruptedException;
}
