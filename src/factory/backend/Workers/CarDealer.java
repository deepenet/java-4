package factory.backend.Workers;

import factory.backend.IdGenerator;
import factory.backend.Products.Car;
import factory.frontend.MainFrame;

public class CarDealer extends Worker implements Runnable {
// fields
    private SellCarController sellCarController;
    private int dealerId;
    private WorkersUnion workersUnion;
// methods
    public CarDealer(int dealerId, IdGenerator idGenerator, SellCarController sellCarController, MainFrame mainFrame, WorkersUnion workersUnion) {
        super(idGenerator, mainFrame);
        this.dealerId = dealerId;
        this.delay = 0;
        this.sellCarController = sellCarController;
        this.workersUnion = workersUnion;
        pushChanges();
    }
    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                Car car = sellCarController.getCar(dealerId);
                workersUnion.workerStart();
                pushChanges();
                Thread.sleep(delay);
                workersUnion.workerStop();
                pushChanges();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void pushChanges() {
        mainFrame.setWaitingDealersNumb(workersUnion.getWaitersNumb());
        mainFrame.setWorkingDealersNumb(workersUnion.getWorkersNumb());
        mainFrame.setDealersProduced(workersUnion.getProduced());
    }
}
