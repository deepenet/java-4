package factory.backend.Workers;

import factory.backend.IdGenerator;
import factory.backend.Products.Accessory;
import factory.backend.Products.Product;
import factory.backend.Storages.AccessoryStorage;
import factory.frontend.MainFrame;

public class AccessorySupplier extends Supplier {
    private MainFrame mainFrame;
    private WorkersUnion workersUnion;
    public AccessorySupplier(IdGenerator idGenerator, AccessoryStorage storage, MainFrame mainFrame, WorkersUnion workersUnion) {
        super(idGenerator, storage, mainFrame);
        this.mainFrame = mainFrame;
        this.workersUnion = workersUnion;
        pushChanges();
    }

    Product doWork() throws InterruptedException {
        workersUnion.workerStart();
        pushChanges();
        Thread.sleep(super.delay);
        workersUnion.workerStop();
        pushChanges();
        return new Accessory(idGenerator.getId());
    }

    private void pushChanges() {
        mainFrame.setWaitingAccessorySuppliersNumb(workersUnion.getWaitersNumb());
        mainFrame.setWorkingAccessorySuppliersNumb(workersUnion.getWorkersNumb());
        mainFrame.setAccessorySupplierProduced(workersUnion.getProduced());
    }
}
