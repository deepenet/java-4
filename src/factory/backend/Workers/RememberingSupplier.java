package factory.backend.Workers;

import factory.backend.IdGenerator;
import factory.backend.Storages.Storage;
import factory.frontend.MainFrame;


public abstract class RememberingSupplier extends Supplier {
    boolean isWorking;
    int produced;
    protected MainFrame mainFrame;
    RememberingSupplier(IdGenerator idGenerator, Storage storage, MainFrame mainFrame) {
        super(idGenerator, storage, mainFrame);
        isWorking = false;
        produced = 0;
        this.mainFrame = mainFrame;
    }
    abstract void pushChanges();
    void rememberWorking() {
        isWorking = true;
        pushChanges();
    }
    void stopWorking() {
        isWorking = false;
        produced++;
        pushChanges();
    }
}
