package factory.backend.Workers;

import factory.backend.Exceptions.IncorrectProductTypeException;
import factory.backend.IdGenerator;
import factory.backend.Products.*;
import factory.backend.Storages.*;
import factory.frontend.MainFrame;

public class CarLinker extends Worker implements Runnable {
// fields
    private Storage accessoryStorage;
    private Storage bodyStorage;
    private Storage motorStorage;
    private Storage carStorage;
    private WorkersUnion workersUnion;
// methods
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Car car = (Car)doWork(
                        (Accessory) accessoryStorage.getProduct(),
                        (Body) bodyStorage.getProduct(),
                        (Motor) motorStorage.getProduct());
                carStorage.putProduct(car);
            } catch (InterruptedException e) {
                return;
            } catch (IncorrectProductTypeException ignored) {}
        }
    }
    public CarLinker(IdGenerator idGenerator, AccessoryStorage accessoryStorage, BodyStorage bodyStorage,
                     MotorStorage motorStorage, CarStorage carStorage, MainFrame mainFrame, WorkersUnion workersUnion) {
        super(idGenerator, mainFrame);
        this.accessoryStorage = accessoryStorage;
        this.bodyStorage = bodyStorage;
        this.motorStorage = motorStorage;
        this.carStorage = carStorage;
        this.workersUnion = workersUnion;
        pushChanges();
    }
    private Product doWork(Accessory accessory, Body body, Motor motor) throws InterruptedException {
        workersUnion.workerStart();
        pushChanges();
        Thread.sleep(delay);
        System.out.println("Car created");
        workersUnion.workerStop();
        pushChanges();
        return new Car(idGenerator.getId(), accessory, body, motor);
    }

    private  void pushChanges() {
        mainFrame.setSleepingCarLinkersNumb(workersUnion.getWaitersNumb());
        mainFrame.setWorkingCarLinkersNumb(workersUnion.getWorkersNumb());
        mainFrame.setCarLinkersProduced(workersUnion.getProduced());
    }
}
