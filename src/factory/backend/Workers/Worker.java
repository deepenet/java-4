package factory.backend.Workers;


import factory.backend.IdGenerator;
import factory.frontend.MainFrame;

public class Worker {
// fields
    volatile long delay;
    IdGenerator idGenerator;
    protected MainFrame mainFrame;
// methods
    Worker(IdGenerator idGenerator, MainFrame mainFrame) {
        this.idGenerator = idGenerator;
        this.mainFrame = mainFrame;
    }
    public void setDelay(long delay) {
        this.delay = delay;
    }
}
