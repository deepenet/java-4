package factory.backend.Workers;

import factory.backend.Products.Car;
import factory.backend.Storages.CarStorage;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.time.Clock;

public class SellCarController {
// fields
    private CarStorage storage;
    private FileWriter logFile;
// methods
    public SellCarController(CarStorage storage, FileWriter logFile) {
        this.storage = storage;
        this.logFile = logFile;
    }
    Car getCar(int dealerId) throws InterruptedException {
        Time time;
        Car car = (Car) storage.getProduct();
        time = new Time(Clock.systemUTC().millis());
        System.out.println("Car taken");
        if(logFile != null) {
            try {
                logFile.write(time.toString() + ": Dealer " + dealerId + ": Car " + car.getId() + "( " +
                        "Body: " + car.getBody().getId() +
                        ", Motor: " + car.getMotor().getId() +
                        ", Accessory: " + car.getAccessory().getId() + " )\n");
                logFile.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return car;
    }
}
